---
title: "7_compAnalysisDec8"
output: html_document
---

Is there a systematic shift in small rodent diet through a population cycle or season that is important for shaping the dynamics?


# Prepare the R environment, load and prepare the data

```{r message=FALSE}
library(tidyverse) # for easier working environment
library(vegan)
library(ggvegan)
library(veganUtils)

dataAll <- read_csv("./../Manuscript/Tables/Table_J_overviewOnAllData_edible_allTaxLevels.csv",show_col_types = FALSE)
```


## Tidy the data

```{r}
#Use animal species instead of habitat for subsetting
dataAll$voleSpecies <- dataAll$Habitat
dataAll[dataAll$voleSpecies == "Ditch",]$voleSpecies <- "tundraVole"
dataAll[dataAll$voleSpecies == "Forest",]$voleSpecies <- "bankVole"

#Add phases and seasons
dataAll <- dataAll %>%
  arrange(date) %>%
  mutate(date = paste(date, "-15", sep = ""),
         date = as.Date(date))

# phase
dataAll$phase <- "peak"
#dataAll$phaseWinterCrash <- "increasePeak"
#dataAll[which(dataAll$date > "2017-10-05"),]$phaseWinterCrash <- "crashLow"
dataAll[which(dataAll$date > "2018-05-05"),]$phase <- "low"

# seasons (reproductive season = summer & non-growing season = winter)
dataAll$season <- "summer"
dataAll[which(dataAll$date > "2017-09-27" & dataAll$date < "2018-05-05"),]$season <- "winter"
dataAll[which(dataAll$date > "2018-09-20" & dataAll$date < "2019-04-27"),]$season <- "winter"
dataAll[which(dataAll$date > "2019-09-20" & dataAll$date < "2020-04-27"),]$season <- "winter"

# return to months
dataAll <- dataAll %>%
  mutate(date = str_remove(date, pattern = "-15"))

# combine predictor variables
dataAll <- dataAll %>% mutate(across(c(phase, season), str_to_sentence)) %>% unite("exp", phase:season, sep = "", remove = FALSE) 
dataAll$exp <- as.factor(dataAll$exp)
dataAll$phase <- as.factor(dataAll$phase)
dataAll$phase <- factor(dataAll$phase, levels=rev(levels(dataAll$phase)))
dataAll$season <- as.factor(dataAll$season)

# grid
grids <- read_delim("sample_desc_grid.csv",  # Add grid id to the sample descriptions
                show_col_types = FALSE) %>%
  distinct() %>% select(code, grid) %>%
  rename(Sample_ID = "code") %>% mutate(across(c(grid), str_to_sentence))
dataAll <- left_join(dataAll,grids, by = "Sample_ID")
dataAll[dataAll$grid == "Dm1",]$grid <- "M1" #on tundra vole habitat
dataAll[dataAll$grid == "Dm2",]$grid <- "M2"
```


### Pivot wider vole specific vasc.plant primer database

Choose predictor variables (exp) or (phase, season)

```{r}
## Bank vole
# RRA per sample
pF <- dataAll %>%
  filter(Primer == "G-H.plant",
         voleSpecies == "bankVole") %>%
   group_by(Sample_ID, genus_name, exp, grid) %>% 
  summarise(sampleRRA = sum(TaxaRRA)) %>%
  arrange(desc(sampleRRA)) %>%
  ungroup()
nSamples <- length(unique(pF$Sample_ID))
pf.RRA <- pF

# Presence/absence per sample (not really FOO...)
pF.FOO <- pF
pF.FOO$sampleRRA[pF.FOO$sampleRRA > 0] <- 1
pF.FOO <- pF.FOO %>%
  group_by(Sample_ID, genus_name, exp, grid) %>%
  tally() %>%
  arrange(genus_name) %>%
  ungroup()

# Pivot wider
plF.RAA <- pf.RRA %>%
  group_by(Sample_ID, genus_name, exp, grid) %>%
  mutate(row = row_number()) %>%
  tidyr::pivot_wider(names_from = genus_name, values_from = sampleRRA) %>%
  select(-row) %>%
  rename("Higher tax.level" = "NA")
plF.RAA[is.na(plF.RAA)] <- 0

plF.FOO <- pF.FOO %>%
  group_by(Sample_ID, genus_name, exp, grid) %>%
  mutate(row = row_number()) %>%
  tidyr::pivot_wider(names_from = genus_name, values_from = n) %>%
  select(-row) %>%
  rename("Higher tax.level" = "NA")
plF.FOO[is.na(plF.FOO)] <- 0


## Tundra vole
# RRA per sample
pT <- dataAll %>%
  filter(Primer == "G-H.plant",
         voleSpecies == "tundraVole") %>%
   group_by(Sample_ID, genus_name, phase, season, grid) %>% 
  summarise(sampleRRA = sum(TaxaRRA)) %>%
  arrange(desc(sampleRRA)) %>%
  ungroup()
nSamples <- length(unique(pT$Sample_ID))
pT.RRA <- pT

# Presence/absence per sample (not really FOO...)
pT.FOO <- pT
pT.FOO$sampleRRA[pT.FOO$sampleRRA > 0] <- 1
pT.FOO <- pT.FOO %>%
  group_by(Sample_ID, genus_name, phase, season, grid) %>%
  tally() %>%
  arrange(genus_name) %>%
  ungroup()

# Pivot wider
plT.RAA <- pT.RRA %>%
  group_by(Sample_ID, genus_name, phase, season, grid) %>%
  mutate(row = row_number()) %>%
  tidyr::pivot_wider(names_from = genus_name, values_from = sampleRRA) %>%
  select(-row) %>%
  rename("Higher tax.level" = "NA")
plT.RAA[is.na(plT.RAA)] <- 0

plT.FOO <- pT.FOO %>%
  group_by(Sample_ID, genus_name, phase, season, grid) %>%
  mutate(row = row_number()) %>%
  tidyr::pivot_wider(names_from = genus_name, values_from = n) %>%
  select(-row) %>%
  rename("Higher tax.level" = "NA")
plT.FOO[is.na(plT.FOO)] <- 0


# plF, n = 127 
# plD, n = 66
```


## Ordination analysis

Separate the data. Can we use presence/absence here as well?

```{r}
plants <- 
  plF.RAA %>%
#  plF.FOO %>% #?
#  plT.RAA %>%
#  plT.FOO %>% #?
  ungroup() %>%
  discard(~ all(is.na(.) | . == "" | . == 0)) # remove empty columns if any

sp <-
  plants %>%
  select(where(is.numeric))

explvars <-
  plants %>%
  select(!where(is.numeric))
```

Perform imputation and transformation

```{r}
# impute zeros
# detlim <- rep(1e-6, ncol(sp))
# sp_imp_res <- robCompositions::impRZilr(sp, dl = detlim) # takes time
# spimp.RAA <- sp_imp_res$x
# saveRDS(spimp.RAA, "spimp.RAA.bankVole.phaseSeason.rds")
spimp.RAA <- readRDS("spimp.RAA.bankVole.phaseSeason.rds") # with 4-level factor
#spimp.RAA <- readRDS("spimp.RAA.bankVoel.rds") # with 2x2 level factors
#spimp.RAA <- readRDS("spimp.RAA.tundraVole.rds") # with 2x2 level factors

# perform ilr transformation
spimp_ilr <- compositions::ilr(spimp.RAA)
colnames(spimp_ilr) <- colnames(sp)[-1]
```


Or, should we rather use Hellinger transformation?

```{r}
sp.hell <- decostand(sp, method = "hellinger")
```



## Perform PCA

```{r}
library(vegan)
library(ggvegan)
library(veganUtils)

### PCA
modPCA.ilr <- rda(spimp_ilr)
modPCA.hell <- rda(sp.hell)
modPCA.ilr
modPCA.hell

screeplot(modPCA.ilr)
screeplot(modPCA.hell)

modPCA.ilr$CA$eig[1] / modPCA.ilr$tot.chi # first axis eigenvalue
modPCA.hell$CA$eig[1] / modPCA.ilr$tot.chi # first axis eigenvalue

biplot(modPCA.ilr)
biplot(modPCA.hell)

ord_plot(modPCA.ilr, title.main = "ilr-transformation")
ord_plot(modPCA.hell, title.main = "hellinger-transformation")
```

The plots are different depending on the transformation, and Hellinger has much lower eigenvalues on axes. How to choose between the two?




## Perform RDA

```{r}
### RDA with ILR transformation
mod.ilr <- rda(spimp_ilr ~ exp + grid, data = explvars) #forced interaction for the sake of interpration
RsquareAdj(mod.ilr)$r.squared # 23 %

#mod.ilr <- rda(spimp_ilr ~ phase + season + grid, data = explvars)
#RsquareAdj(mod.ilr)$r.squared # 21 %

#mod.ilr <- rda(spimp_ilr ~ exp, data = explvars) #forced interaction for the sake of interpration
#RsquareAdj(mod.ilr)$r.squared # 11 %

#mod.ilr <- rda(spimp_ilr ~ phase + season, data = explvars)
#RsquareAdj(mod.ilr)$r.squared # 8 %

mod.ilr
anova(mod.ilr)
#summary(mod.ilr)
```


## Table to report result

```{r}
# Scree plot
screeplot(mod.ilr)

# Sample size
127 #bank voles

# inertia ratio (Sum of Eigenvalues RDA divided by sum of eigenvalues PCA (three first axes))
sum(mod.ilr$CCA$eig[1:3]) / sum(modPCA.ilr$CA$eig[1:3]) # 0.39

# Eigenvalue divided by total egeinvalues to find how much variance is explained?
modPCA.ilr$CA$eig[1] / sum(modPCA.ilr$CA$eig) # 0.21
modPCA.ilr$CA$eig[2] / sum(modPCA.ilr$CA$eig) # 0.15
modPCA.ilr$CA$eig[3] / sum(modPCA.ilr$CA$eig) # 0.12

mod.ilr$CCA$eig[1] / sum(mod.ilr$CCA$eig) # 0.46
mod.ilr$CCA$eig[2] / sum(mod.ilr$CCA$eig) # 0.20
mod.ilr$CCA$eig[3] / sum(mod.ilr$CCA$eig) # 0.14

# cumulative % variance of diet
modPCA.ilr$CA$eig[1] / sum(modPCA.ilr$CA$eig) # 0.21

modPCA.ilr$CA$eig[1] / sum(modPCA.ilr$CA$eig) + 
  modPCA.ilr$CA$eig[2] / sum(modPCA.ilr$CA$eig) # 0.36

modPCA.ilr$CA$eig[1] / sum(modPCA.ilr$CA$eig) +
  modPCA.ilr$CA$eig[2] / sum(modPCA.ilr$CA$eig) +
  modPCA.ilr$CA$eig[3] / sum(modPCA.ilr$CA$eig) # 0.48
```



## Plot RDA

```{r}
#pdf(file="./../Manuscript/Figures/FigureRDA_Bank_inter.pdf")

ord_plot(mod.ilr,
         xlim = c(-3.5, 3.5),
         ylim = c(-3.5, 3.5))

#dev.off()
```


```{r}
library(veganUtils)
axis_plot(mod.ilr, italicize_sp_names = TRUE)
```


# Tundra vole

Select tundra vole data, perform imputation and transformation

```{r}
plants <- 
  plT.RAA %>% #tundra voles
  ungroup() %>%
  discard(~ all(is.na(.) | . == "" | . == 0)) # remove empty columns if any

sp <-
  plants %>%
  select(where(is.numeric))

explvars <-
  plants %>%
  select(!where(is.numeric))


# impute zeros
# detlim <- rep(1e-6, ncol(sp))
# sp_imp_res <- robCompositions::impRZilr(sp, dl = detlim) # takes time
# spimp.RAA <- sp_imp_res$x
spimp.RAA <- readRDS("spimp.RAA.tundraVole.rds") # with 2x2 level factors

# perform ilr transformation
spimp_ilr <- compositions::ilr(spimp.RAA)
colnames(spimp_ilr) <- colnames(sp)[-1]
```

## Perform PCA

```{r}
library(vegan)
library(ggvegan)
library(veganUtils)

### PCA
modPCA.ilr <- rda(spimp_ilr)
modPCA.ilr

screeplot(modPCA.ilr)

modPCA.ilr$CA$eig[1] / modPCA.ilr$tot.chi # first axis eigenvalue, 24 %

ord_plot(modPCA.ilr, title.main = "ilr-transformation")
```

## Perform RDA

```{r}
### RDA with ILR transformation
mod.ilr <- rda(spimp_ilr ~ phase + season + grid, data = explvars)
RsquareAdj(mod.ilr)$r.squared # 14 %

#mod.ilr <- rda(spimp_ilr ~ phase + season, data = explvars)
#RsquareAdj(mod.ilr)$r.squared # 6 %

mod.ilr
anova(mod.ilr)
#summary(mod.ilr)
```


## Table to report result

```{r}
# Scree plot
screeplot(mod.ilr)

# Sample size
66 #tundra voles

# inertia ratio (Sum of Eigenvalues RDA divided by sum of eigenvalues PCA (three first axes))
sum(mod.ilr$CCA$eig[1:3]) / sum(modPCA.ilr$CA$eig[1:3]) # 0.30

# Eigenvalue divided by total egeinvalues to find how much variance is explained?
modPCA.ilr$CA$eig[1] / sum(modPCA.ilr$CA$eig) # 0.24
modPCA.ilr$CA$eig[2] / sum(modPCA.ilr$CA$eig) # 0.14
modPCA.ilr$CA$eig[3] / sum(modPCA.ilr$CA$eig) # 0.10

mod.ilr$CCA$eig[1] / sum(mod.ilr$CCA$eig) # 0.61
mod.ilr$CCA$eig[2] / sum(mod.ilr$CCA$eig) # 0.25
mod.ilr$CCA$eig[3] / sum(mod.ilr$CCA$eig) # 0.14

# cumulative % variance of diet
modPCA.ilr$CA$eig[1] / sum(modPCA.ilr$CA$eig) # 0.24

modPCA.ilr$CA$eig[1] / sum(modPCA.ilr$CA$eig) + 
  modPCA.ilr$CA$eig[2] / sum(modPCA.ilr$CA$eig) # 0.38

modPCA.ilr$CA$eig[1] / sum(modPCA.ilr$CA$eig) +
  modPCA.ilr$CA$eig[2] / sum(modPCA.ilr$CA$eig) +
  modPCA.ilr$CA$eig[3] / sum(modPCA.ilr$CA$eig) # 0.48
```

## Plot RDA

```{r}
#pdf(file="./../Manuscript/Figures/FigureRDA_Tundra.pdf")

ord_plot(mod.ilr,
         xlim = c(-2.5, 2.5),
         ylim = c(-2.5, 2.5))

#dev.off()
```


```{r}
library(veganUtils)
axis_plot(mod.ilr, italicize_sp_names = TRUE)
```



