

library(tidyverse)
# library(Compositional)
library(robCompositions)


load("dataForCCA/responseVariables_edible2.RData")

hab <- "Ditch"

plants <-
  plantGen %>%
  filter(Habitat == hab) %>%
  discard(~ all(is.na(.) | . == "" | . == 0))






# direct
pldir <-
  plants %>%
  filter(TypeOfSampling == "Direct") %>%
  select(where(is.numeric))

# indirect
plindir <-
  plants %>%
  filter(TypeOfSampling == "Indirect") %>%
  select(where(is.numeric))



# keep species in at least 10 percent of observations
pobs <- 0.05

pldir <-
  pldir %>%
  select(where(~ sum(.x != 0) > pobs * length(.x)))

plindir <-
  plindir %>%
  select(where(~ sum(.x != 0) > pobs * length(.x)))

dim(pldir)
dim(plindir)

sptokeep <- intersect(names(pldir), names(plindir))

pldir <-
  pldir %>%
  select(all_of(sptokeep))

plindir <-
  plindir %>%
  select(all_of(sptokeep))

# check
identical(names(pldir), names(plindir))


# impute
detlim <- rep(1e-6, ncol(pldir))
pldir_imp_res <- impRZilr(pldir, dl = detlim)
pldirimp <- pldir_imp_res$x

detlim <- rep(1e-6, ncol(plindir))
plindir_imp_res <- impRZilr(plindir, dl = detlim)
plindirimp <- plindir_imp_res$x

# check
identical(names(pldirimp), names(plindirimp))


# correlation between direct and indirect is ~ 0.76
# so we can consider they are the same information and use them together


# go back to whole table (direct + indirect)



hab <- "Ditch"

plants <-
  plantGen %>%
  filter(Habitat == hab) %>%
  discard(~ all(is.na(.) | . == "" | . == 0))

sp <-
  plants %>%
  select(where(is.numeric))

explvars <-
  plants %>%
  select(!where(is.numeric))

detlim <- rep(1e-6, ncol(sp))

sp_imp_res <- impRZilr(sp, dl = detlim)

spimp <- sp_imp_res$x


library(compositions)

spimp_ilr <- ilr(spimp)

saveRDS(spimp, "spimp.rds")


library(vegan)


rda_res <- rda(spimp_ilr ~ phase + season, data = explvars)
plot(rda_res)

cca_res <- cca(spimp ~ phase + season, data = explvars)
plot(cca_res)


















