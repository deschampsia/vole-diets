---
title: "Vole diet paper (5/x)"
#output: html_notebook: default
---

## Prepare the R environment

```{r message=FALSE}
library(tidyverse) # for easier working environment
library(vegan) # for diversity index functions
source("0_helperScript.R") #contains custom functions
```

## Load the data

```{r}
dataAll <- read_csv("./../Manuscript/Tables/Table_J_overviewOnAllData_edible_allTaxLevels.csv", show_col_types = FALSE)

dataAll$voleSpecies <- dataAll$Habitat # separating between species makes more sense than habitat (same thing in this case)
dataAll[dataAll$voleSpecies == "Ditch",]$voleSpecies <- "tundraVole"
dataAll[dataAll$voleSpecies == "Forest",]$voleSpecies <- "bankVole"

dataAll$Sample_ID <- as.factor(dataAll$Sample_ID)
```

Add phases and seasons
```{r}
dataAll <- dataAll %>%
  arrange(date) %>%
  mutate(date = paste(date, "-15", sep = ""),
         date = as.Date(date))

# phase
dataAll$phase <- "increasePeak"
dataAll$phaseWinterCrash <- "increasePeak"
dataAll[which(dataAll$date > "2017-10-05"),]$phaseWinterCrash <- "crashLow"
dataAll[which(dataAll$date > "2018-05-05"),]$phase <- "crashLow"

# seasons (reproductive season = summer & non-growing season = winter)
dataAll$season <- "summer"
dataAll[which(dataAll$date > "2017-09-27" & dataAll$date < "2018-05-05"),]$season <- "winter"
dataAll[which(dataAll$date > "2018-09-20" & dataAll$date < "2019-04-27"),]$season <- "winter"
dataAll[which(dataAll$date > "2019-09-20" & dataAll$date < "2020-04-27"),]$season <- "winter"

# return to months
dataAll <- dataAll %>%
  mutate(date = str_remove(date, pattern = "-15"))

dataAll$phase <- as.factor(dataAll$phase)
dataAll$phase <- factor(dataAll$phase, levels=rev(levels(dataAll$phase)))
```

Keep vasc.plants. Separate vole species

```{r}
# Keep vasc.plants, on genus level
plantGen <- dataAll %>%
  filter(Primer == "G-H.plant") %>%
  select(Sample_ID, phase, phaseWinterCrash, season, TypeOfSampling, voleSpecies, genus_name, date, TaxaRRA) %>%
  group_by(genus_name) %>%
  mutate(row = row_number()) %>%
  tidyr::pivot_wider(names_from = genus_name, values_from = TaxaRRA) %>%
  select(-row) %>%
  
  select_if(function(x){any(!is.na(x))}) %>% # removes empty columns if any
  select(sort(names(.))) %>% # sorts by column name
  select(Sample_ID, phase, phaseWinterCrash, season, TypeOfSampling, voleSpecies, date, everything()) %>% 
  group_by(Sample_ID, phase, phaseWinterCrash, season, TypeOfSampling, voleSpecies, date) %>%
  summarise_if(is.numeric, sum, na.rm = TRUE) %>% ungroup() ; plantGen[is.na(plantGen)] <- 0
```

Calculate diversity index per sample

```{r}
x <- plantGen %>%
  select(where(is.numeric))

plantDiv <- plantGen %>%
  select(!where(is.numeric)) %>%
  add_column(shannon = diversity(x, index = "shannon")) %>%
  add_column(invS = diversity(x, index = "inv"))
```

The exponent of the Shannon index is linearly related to inverse Simpson (Hill 1973) although the former may be more sensitive to rare species (Oksanen in Vegan manual). However Shannon is more robust with complex datasets (Morris et al., 2014 EcolEvol).


Subset, plot and explore

```{r}
plantDiv$voleSpecies %>%
  table()
# need to downsample here?


# vole species
plantDiv %>%
  ggplot(aes(voleSpecies, invS)) + 
  geom_boxplot() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  theme_classic()

# season
plantDiv %>%
  filter(voleSpecies == "bankVole") %>%

  ggplot(aes(season, invS)) + 
  geom_boxplot() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  theme_classic()

plantDiv %>%
  filter(voleSpecies == "tundraVole") %>%
  
  ggplot(aes(season, invS)) + 
  geom_boxplot() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  theme_classic()

# season (month)
plantDiv %>%
  filter(voleSpecies == "bankVole") %>%
  mutate(month = str_sub(date, 6, 7)) %>%
  
  ggplot(aes(month, invS)) + 
  geom_boxplot() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  theme_classic() +
  ggtitle("A)") +
  ylab("Diversity index") + xlab("Phase") -> A ; A

plantDiv %>%
  filter(voleSpecies == "tundraVole") %>%
  mutate(month = str_sub(date, 6, 7)) %>%
  
  ggplot(aes(month, invS)) + 
  geom_boxplot() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  theme_classic()

# phase
plantDiv %>%
  filter(voleSpecies == "bankVole") %>%
  
  ggplot(aes(phase, invS)) + 
  geom_boxplot() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  theme_classic()

plantDiv %>%
  filter(voleSpecies == "tundraVole") %>%
  
  ggplot(aes(phase, invS)) + 
  geom_boxplot() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  theme_classic() +
  ggtitle("B)") +
  ylab("Diversity index") + xlab("Phase") -> B ; B

ggsave("./../Manuscript/Figures/Figure_R_diversityPlotPhaseSeason.jpg", grid.arrange(A, B, ncol = 2),
       width = 10, height = 5)



# in summary
plantDiv %>% filter(voleSpecies == "bankVole") %>%
  summarise(b_mean = mean(invS),
            se = se(invS))

plantDiv %>% filter(voleSpecies == "tundraVole") %>%
  summarise(t_mean = mean(invS),
            se = se(invS))
```


Test diversity measures with univariate model, each vole species separately

```{r}
library(modelbased)
library(see)


# For table output 
model <- glm(invS ~ phase + season,
             family = "poisson",
             data = plantDiv %>% filter(voleSpecies == "bankVole")
             )
library(DHARMa)
r <- simulateResiduals(model)
plot(r)


# Estimate marginal contrasts
contrasts <- estimate_contrasts(model)
contrasts
#
model <- glm(invS ~ phase + season,
             family = "poisson",
             data = plantDiv %>% filter(voleSpecies == "tundraVole")
             )
# Estimate marginal contrasts
contrasts <- estimate_contrasts(model)
contrasts
```













